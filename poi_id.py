# %load poi_id.py
#!/usr/bin/python 

import sys
import pickle
import numpy as np
import pandas as pd
import math
import locale
from IPython.display import Image
import matplotlib.pyplot as plt
from sklearn import preprocessing
from time import time
from sklearn.naive_bayes import GaussianNB
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.svm import SVC
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
import matplotlib.patches as mpatches
from sklearn.feature_selection import SelectKBest, f_classif
from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
sys.path.append("../tools/")


### Load the dictionary containing the dataset
with open("final_project_dataset.pkl", "r") as data_file:
    data_dict = pickle.load(data_file)

### Store to my_dataset for easy export below.
my_dataset = data_dict


print "NOTE: My final condensed code is in this file, but my workflow, explanations, and reasoning for the code below "
print "are located in the read.me file and HTML file\n"

print "Task 1: Remove outliers\n"

my_dataset.pop('TOTAL',0)
my_dataset.pop('THE TRAVEL AGENCY IN THE PARK',0)
my_dataset.pop('LOCKHART EUGENE E',0)
print

print "Task 2: Create new feature(s)."

print "I created four new features using fractions:\n"

### New features are: frac_to_poi_email, frac_from_poi_email, frac_salary_to_total_pay, frac_bonus_to_total_pay
def frac_list(key,normalizer):
    new_list=[]

    for name in my_dataset:
        if my_dataset[name][key]=="NaN" or my_dataset[name][normalizer]=="NaN":
            new_list.append(0.)
        elif my_dataset[name][key]>=0:
            new_list.append(float(my_dataset[name][key])/float(my_dataset[name][normalizer]))
    return new_list

frac_from_poi_email = frac_list("from_poi_to_this_person","to_messages")
frac_to_poi_email = frac_list("from_this_person_to_poi","from_messages")
frac_salary_to_total_pay = frac_list("salary","total_payments")
frac_bonus_to_total_pay = frac_list("bonus","total_payments")

print "Insert the new features into copy of data_dict\n"
count=0
for name in my_dataset:
    my_dataset[name]["frac_from_poi_email"]=frac_from_poi_email[count]
    my_dataset[name]["frac_to_poi_email"]=frac_to_poi_email[count]
    my_dataset[name]["frac_salary_to_total_pay"]=frac_salary_to_total_pay[count]
    my_dataset[name]["frac_bonus_to_total_pay"]=frac_bonus_to_total_pay[count]
    count +=1
    

print "Task 3: Select what features you'll use.\n"

### features_list is a list of strings, each of which is a feature name.
### The first feature must be "poi".

############# List original and new features and labels from dataset 
orig_and_new_features = ['poi','salary', 'to_messages', 'deferral_payments', 'total_payments', 'loan_advances', 'bonus', 'restricted_stock_deferred', 'deferred_income', 'total_stock_value', 'expenses', 'from_poi_to_this_person', 'exercised_stock_options', 'from_messages', 'other', 'from_this_person_to_poi', 'long_term_incentive', 'shared_receipt_with_poi', 'restricted_stock', 'director_fees', 'frac_to_poi_email', 'frac_from_poi_email', 'frac_salary_to_total_pay', 'frac_bonus_to_total_pay']

print "In order to use the KBest feature selector, we need a starting set labels and features numpy arrays... "
print "use original feature list to generate them.\n"
data = featureFormat(my_dataset, orig_and_new_features, sort_keys = True)
labels, features = targetFeatureSplit(data)

#DEBUG: 
#print labels

print "Use KBest to find top KBest features\n" 

###
### It turned out that after evaluating k= 'all', 2,3,4,5,6,7,8,9,to that 'all' was the best predictor
###
# create an instance of the KBest method:
kbest = SelectKBest()

print "if there are specific options that you want, you can include them in the parentheses:"
print "i.e. select the best features using 'SelectKBest' \n"
kbest = SelectKBest(f_classif, k='all')

print "Fit the method (or in the case of processing the data, fit and transform at the same time)."
print "Selected_features are the features selected by SelectKbest\n" 
selected_features = kbest.fit_transform(features, labels)
# DEBUG
#print selected_features, "\n"


print "Create the final features list using KBest info\n"
features_list = [orig_and_new_features[i+1] for i in (kbest.get_support(indices=True))]
# DEBUG
#print features_list
# add 'poi' to start of list
features_list = ['poi'] + features_list
print "\nFinal features list:\n\n", features_list


print "Create the final numpy arrays for labels and features using the KBest features list\n"

data_kb = featureFormat(data_dict, features_list, sort_keys = True)
# Note that train and test labels are the same, unchanged. No need to load them
labels, features = targetFeatureSplit(data_kb)

# DEBUG
#print labels
#print len(features)

print "Task 4: Try a varity of classifiers\n"

print "Prepare to train/test. Use KBEST features...\n"

print "NOTE: Tester.py appears to use 14000 iterations to determine precision and recall... also a random test selection %"
print "So, I'm going to pick 50% test split as a best guess to train as opposed to the typical 30%.\n"

features_train,features_test,labels_train,labels_test = train_test_split(features, labels, test_size=0.5, random_state=42)


print "Naive Bayes default classifier\n"

clf = GaussianNB()

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

print "Naive Bayes Local Recall = \t\t", recall_score(labels_test, pred)
print "Naive Bayes Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "Naive Bayes Confusion Matrix:\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)

print "Surprisingly to me, Naive Bayes default meets min. requirements. Tester.py  Precision: 0.38355 Recall: 0.31700\n\n"

print "KNeighbors default"

# Create and fit an  
clf = KNeighborsClassifier()

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

print "KNeighborsClassifier Local Recall = \t\t", recall_score(labels_test, pred)
print "KNeighborsClassifier Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "KNeighborsClassifier Confusion Matrix:\n\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)

print "KNeighbors default does not meet  min. requirements. Tester.py precision = 0.41632, recall = 0.09950.\n\n" 

print "RandomForestClassifier default"

 
from sklearn.ensemble import RandomForestClassifier 

# Create and fit an AdaBoosted decision tree
clf = RandomForestClassifier()

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

print "RandomForestClassifier Local Recall = \t\t", recall_score(labels_test, pred)
print "RandomForestClassifier Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "RandomForestClassifier Confusion Matrix:\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)

print "RandomForestClassifier default does not meet  min. requirements. Tester.pys precision: 0.41349, recall: 0.14100\n\n"

print "ADABOOST Default"

print "Create and fit an AdaBoost default decision tree"
clf = AdaBoostClassifier()

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

print "AdaBoostClassifier Default Local Recall = \t\t", recall_score(labels_test, pred)
print "AdaBoostClassifier Default Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "AdaBoostClassifier Default Confusion Matrix:\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)

print "AdaBoost default does not meet  min. requirements. Tester.pys precision: 0.31855	recall: 0.25850\n\n"

print "DECISION TREE default" 


clf = tree.DecisionTreeClassifier()

print "Tried manually to pick DT params, but could never get recall and precision above .3 together\n\n"  
#clf = tree.DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
#              max_features=None, max_leaf_nodes=None, min_samples_leaf=1,
#              min_samples_split=2, min_weight_fraction_leaf=0.0,
#              random_state=None, splitter='best')




clf.fit(features_train, labels_train)

pred = clf.predict(features_test)

# Recall and Precision are based on true vs predicted
                             
print "DecisionTreeClassifier Local DT Recall = \t\t", recall_score(labels_test, pred)
print "DecisionTreeClassifier Local DT Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "DecisionTreeClassifier Confusion Matrix:\n\n", cm

print "DecisionTreeClassifier default does not meet  min. requirements. Tester.py Precision: 0.27035 Recall: 0.28900  \n\n"


dump_classifier_and_data(clf, my_dataset, features_list)

print "Task 5: Tune your classifier(s) to achieve better than .3 precision and recall\n\n"

print "Decision Tree manually tuned" 

print "I spent several hours manually tuning the Decision Tree algorithm below. I was unsuccessful and frustrated and I" 
print "was getting ready to learn about using pipeline and GridSearchCV when I tried tuning AdaBoost below, and that worked"
print "(met requirements), so I stopped working on the DT algo.\n\n"


clf = tree.DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
              max_features=6, max_leaf_nodes=None, min_samples_leaf=1,
              min_samples_split=3, min_weight_fraction_leaf=0.0,
              random_state=None, splitter='best')


clf.fit(features_train, labels_train)

pred = clf.predict(features_test)

accuracy = accuracy_score(pred, labels_test)
print "Decision Tree manually tuned Accuracy = \t\t", (accuracy)

# Recall and Precision are based on true vs predicted
                             
print "Decision Tree manually tuned Local Recall = \t\t", recall_score(labels_test, pred)
print "Decision Tree manually tuned Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "Decision Tree manually tuned Confusion Matrix:\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)


print "DECISION TREE manually tuned tester.py results : does not meet min. requirements. Precision = 0.28795, recall = 0.26650\n\n"

###  ---

print "ADABOOST final tuning MEETS meets min. requirements of recall & precision > .3\n\n "

print "Create and fit an AdaBoosted decision tree\n"
clf = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
                         algorithm="SAMME", learning_rate=1,
                         n_estimators=5)

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

print "Tuned AdaBoostClassifier Local Recall = \t\t", recall_score(labels_test, pred)
print "Tuned AdaBoostClassifier Local Precision = \t\t", precision_score(labels_test, pred) 

cm = confusion_matrix(labels_test, pred)
print "Tuned AdaBoostClassifier Confusion Matrix:\n", cm

dump_classifier_and_data(clf, my_dataset, features_list)



print "\nADABOOST final tuning MEETS tester.py min. requirements. Precision: 0.43061, recall: 0.31650\n\n"
print "Because this is the final classifier in this file, the working files clf, my_datset, and feautures_list are"
print "written to the final_project directory and used by TESTER.PY for final validation / evaluation.\n\n"  



### using our testing script. Check the tester.py script in the final project
### folder for details on the evaluation method, especially the test_classifier
### function. Because of the small size of the dataset, the script uses
### stratified shuffle split cross validation. For more info: 
### http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html
