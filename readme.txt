ENRON Final Project Peter Bakke Readme.txt file

My work process is explained in the Jupyter file:
"ENRON-POI-DETECTION-PROJECT-WORKFLOW-PETER-BAKKE.ipynb"

Notes and references are in this file:
"Notes and references ENRON final Project Peter Bakke.txt"

The final poi_id.py file is provided.

tester.py was run to make sure my sucessful classifier
(AdaBoost) passed the minimum requirements of .3
for both recall and precision. 

Thank you. 
Peter Bakke
